require 'test_helper'

class PagesControllerTest < ActionController::TestCase
  test "should get about" do
    get :about
    assert_response :success
  end

  test "should get term" do
    get :term
    assert_response :success
  end

  test "should get contact" do
    get :contact
    assert_response :success
  end

  test "should get ad" do
    get :ad
    assert_response :success
  end

end
