{
  th: {
    date: {
      formats: {
        short: -> (date, _) { "%d %b #{(date.year + 543).to_s[2, 2]}" }
      }
    },
    'lot-one' => 'รางวัลที่ 1',
    'lot-two' => 'เลขท้าย 2 ตัว',
    'lot-three' => 'เลขท้าย 3 ตัว',
    'near-one' => 'ท่านถูกรางวัลข้างเคียงรางวัลที่ 1',
    'second-rew' => 'ท่านถูกรางวัลที่ 2',
    'third-rew' => 'ท่านถูกรางวัลที่ 3',
    'forth-rew' => 'ท่านถูกรางวัลที่ 4',
    'fifth-rew' => 'ท่านถูกรางวัลที่ 5',
    views: {
      pagination: {
        first: "&laquo;",
        last: "&raquo;",
        previous: "&lsaquo;",
        next: "&rsaquo;",
        truncate: "&hellip;"
      }
    }
  }
}
