Rails.application.routes.draw do
  get '/about' => 'pages#about'
  post '/about' => 'pages#ad'

  resources :tickets
  resources :articles
  root 'tickets#index'
end
