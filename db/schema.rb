# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150718123323) do

  create_table "artciles", force: :cascade do |t|
    t.string   "title",      limit: 255,               null: false
    t.text     "content",    limit: 65535,             null: false
    t.text     "summary",    limit: 65535,             null: false
    t.text     "thumbnail",  limit: 65535
    t.text     "tag",        limit: 65535
    t.integer  "category",   limit: 4,     default: 0
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  create_table "articles", force: :cascade do |t|
    t.string   "title",      limit: 255,               null: false
    t.text     "content",    limit: 65535,             null: false
    t.text     "summary",    limit: 65535,             null: false
    t.text     "thumbnail",  limit: 65535
    t.text     "tag",        limit: 65535
    t.integer  "category",   limit: 4,     default: 0
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  create_table "tickets", force: :cascade do |t|
    t.date     "won_at"
    t.string   "label",      limit: 255
    t.text     "val",        limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "tickets", ["won_at"], name: "index_tickets_on_won_at", using: :btree

end
