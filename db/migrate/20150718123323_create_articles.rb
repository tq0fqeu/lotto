class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :title, null: false
      t.text :content, null: false
      t.text :summary, null: false
      t.text :thumbnail
      t.text :tag
      t.integer :category, default: 0

      t.timestamps null: false
    end
  end
end
