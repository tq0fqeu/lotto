class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.date :won_at
      t.string :label
      t.text :val

      t.timestamps null: false
    end

    add_index :tickets, :won_at
  end
end
