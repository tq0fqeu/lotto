# Public Class: TicketsController
class TicketsController < ApplicationController
  def index
    if params[:won_at].present? && params[:number].present?
      redirect_to "/tickets/#{params[:won_at]}?number=#{params[:number]}"
    else
      @ticket = Ticket.order(won_at: :desc).first
      render :show
    end
  end

  def show
    @ticket = Ticket.find_by_won_at params[:id]
    @results = @ticket.results(params[:number]) if params[:number].present?
  end
end
