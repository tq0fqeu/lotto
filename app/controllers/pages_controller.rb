# Public Class: PagesController
class PagesController < ApplicationController
  skip_before_filter :verify_authenticity_token, only: [:ad]
  def about
  end

  def ad
    @accept = true
    render :about
  end
end
