# Module: TicketsHelper
module TicketsHelper
  def ticket_list(amount = 5)
    Ticket.order(won_at: :desc).limit(amount)
  end

  def ticket_rews(ticket)
    {
      second: { label: 2, amount: 5, bonus: '100,000' },
      third: { label: 3, amount: 10, bonus: '40,000' },
      forth: { label: 4, amount: 50, bonus: '20,000' },
      fifth: { label: 5, amount: 100, bonus: '10,000' }
    }.reduce({}) do |rews, (k, v)|
      rews[k] = v.merge number: ticket.details["#{k}-rew"].each_slice(5).to_a
      rews
    end
  end
end
