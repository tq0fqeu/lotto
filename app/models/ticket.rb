# Public Class: Ticket
class Ticket < ActiveRecord::Base
  def details
    @details ||= JSON.parse val
    @details
  end

  def results(number)
    [
      'lot-one', 'lot-two', 'lot-three', 'near-one',
      'second-rew', 'third-rew', 'forth-rew', 'fifth-rew'
    ].each_with_object([]) do |k, m|
      numbers = details[k]
      numbers = numbers.split(' ') unless numbers.is_a? Array
      numbers.each do |n|
        next unless number =~ /#{n}$/
        m.push k
        break
      end
      m
    end
  end
end
