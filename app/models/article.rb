# Public Class: Article
class Article < ActiveRecord::Base
  enum category: [:predict, :dream]

  validates :title, :content, :summary, uniqueness: true
end
